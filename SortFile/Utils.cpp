#include <windows.h>
#include <stdio.h>
#include <vector>
#include "Utils.h"

__int64 GetFileSize64( LPCWSTR File )
{
	WIN32_FILE_ATTRIBUTE_DATA fData;
	LARGE_INTEGER size;

	int res = GetFileAttributesEx( File, GetFileExInfoStandard, &fData );
	if( res != 0 )
	{
		size.HighPart = fData.nFileSizeHigh;
		size.LowPart = fData.nFileSizeLow;

		return size.QuadPart;
	}

	return 0;
}

void mDeleteFile( wchar_t *DelFile )
{
	if( DeleteFile( DelFile ) == FALSE && GetLastError() == ERROR_SHARING_VIOLATION )
		wprintf( L"Unable to delete file: %s\n", DelFile );

}

void PrintError( void )
{
	LPVOID lpMsgBuf;
	DWORD dw = GetLastError();

	SetLastError( 0 );

	FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, dw, MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ), ( LPTSTR )&lpMsgBuf, 0, NULL );

	wprintf( L"Errorcode %i: %s\n", dw, lpMsgBuf );

	LocalFree( lpMsgBuf );
}

std::wstring& replaceAll( std::wstring& context, std::wstring const& from, std::wstring const& to )
{
	std::size_t lookHere = 0;
	std::size_t foundHere;
	while( ( foundHere = context.find( from, lookHere ) ) != std::string::npos )
	{
		context.replace( foundHere, from.size(), to );
		lookHere = foundHere + to.size();
	}

	return context;
}