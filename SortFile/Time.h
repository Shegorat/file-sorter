#pragma once
#include <Windows.h>
#include <cinttypes>

struct t_bench
{
	void start()
	{
		QueryPerformanceFrequency( &freq_ );
		QueryPerformanceCounter( &start_time_ );
	}

	void end()
	{
		QueryPerformanceCounter( &end_time_ );
	}

	uint64_t microseconds() const
	{
		return ( end_time_.QuadPart - start_time_.QuadPart ) * 1'000'000 / freq_.QuadPart;
	}

	uint64_t milliseconds() const
	{
		return microseconds() / 1'000;
	}

	uint64_t seconds() const
	{
		return microseconds() / 1'000'000;
	}

	double f_microseconds() const
	{
		return ( double )( end_time_.QuadPart - start_time_.QuadPart ) * 1'000'000.0 / freq_.QuadPart;
	}

	double f_milliseconds() const
	{
		return f_microseconds() / 1'000.0;
	}

	double f_seconds() const
	{
		return f_microseconds() / 1'000'000.0;
	}

	void reset()
	{
		start_time_.QuadPart = 0;
		end_time_.QuadPart = 0;
		freq_.QuadPart = 0;
	}

	void restart()
	{
		reset();
		start();
	}

private:
	LARGE_INTEGER start_time_ = { 0, 0 };
	LARGE_INTEGER end_time_ = { 0, 0 };
	LARGE_INTEGER freq_ = { 0, 0 };
};